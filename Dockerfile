#
# Build go project
#
FROM golang:1.15-alpine as go-builder

WORKDIR /go/src/github.com/cntu.cnsl/sidecar-debugger/

COPY . .

RUN go build

#
# Runtime container
#
FROM alpine:latest

# RUN mkdir -p /app && \
#    addgroup -S app && adduser -S app -G app && \
#    chown app:app /app

RUN mkdir -p /app

RUN apk add fuse

WORKDIR /app

COPY --from=go-builder /go/src/github.com/cntu.cnsl/sidecar-debugger/sidecar-debugger .