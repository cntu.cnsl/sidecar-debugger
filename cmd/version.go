package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
)

func init() {
	rootCmd.AddCommand(versionCmd)
}

var versionCmd = &cobra.Command{
	Use:   "version",
	Short: "Print the version number of sidecar-debugger app",
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("Sidecar Debugger Project v0.1")
	},
}
