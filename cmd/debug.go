/*
Command that is used to implement the syscall debugging and emulation

The syntax of command is as follows:

	./program debug <command>

	Example: ./program debug ls .

This code is referenced from LizRice article: debugger-from-scratch (github)

*/

package cmd

import (
	"fmt"
	"os"
	"os/exec"
	"syscall"

	"github.com/fatih/color"
	sec "github.com/seccomp/libseccomp-golang"
	"github.com/spf13/cobra"
)

func init() {
	rootCmd.AddCommand(debugCmd)
	debugCmd.Flags().Int("confine", 0, "0: original, 1: blocked, 2: enosys, 3:sysemu")
}

var debugCmd = &cobra.Command{
	Use:   "debug",
	Short: "Inspect a file in debug mode with security",
	Run: func(cmd *cobra.Command, args []string) {
		confineType, _ := cmd.Flags().GetInt("confine")
		Debug(confineType, args...)
	},
}

func Debug(confineType int, args ...string) {
	var regs syscall.PtraceRegs
	var count = 0
	fmt.Printf("Run %v\n", args)

	cmd := exec.Command(args[0], args...)
	cmd.Stderr = os.Stderr
	cmd.Stdin = os.Stdin
	cmd.Stdout = os.Stdout
	cmd.SysProcAttr = &syscall.SysProcAttr{
		Ptrace: true,
	}
	cmd.Start()
	err := cmd.Wait()
	if err != nil {
		fmt.Printf("Wait returned: %v\n", err)
	}

	pid := cmd.Process.Pid
	exit := true

	for {
		// list through syscalls
		//if exit {
		// Get register of the process
		err = syscall.PtraceGetRegs(pid, &regs)
		if err != nil {
			break
		}

		// Get information of the register
		name, _ := sec.ScmpSyscall(regs.Orig_rax).GetName()
		switch name {
			// In case of data collection to terminal
			case "write":
				if confineType == 0 {
					// Normal
					color.Set(color.FgGreen)
					fmt.Printf("%d %s\n", count, name)
					color.Unset()
					err = syscall.PtraceSyscall(pid, 0)
					if err != nil {
						panic(err)
					}
				} else if confineType == 1 {	
					// Block system call by killing the process
					syscall.Kill(pid, syscall.SIGKILL)
				} else if confineType == 2 {
					// No system call return 
					fmt.Printf("%d %s (No syscall)\n", count, name)
					regs.Orig_rax = uint64(syscall.ENOSYS); 
					regs.Rax = uint64(syscall.EPERM); // Operation not permitted
					err = syscall.PtraceSetRegs(pid, &regs)
					if err != nil {
						break
					}
					err = syscall.PtraceSyscall(pid, 0)
					if err != nil {
						panic(err)
					}
				} else {
				// Emulated
					color.Set(color.FgRed)
					fmt.Printf("%d %s (emulated)\n", count, name)
					color.Unset()
					if _, _, errno := syscall.RawSyscall6(syscall.SYS_PTRACE, syscall.PTRACE_SYSEMU, uintptr(pid), 0, 0, 0, 0); errno != 0 {
						panic(fmt.Sprintf("ptrace syscall-enter failed: %v", errno))
					}
				}
			default:
				/* Real call */
				color.Set(color.FgGreen)
				fmt.Printf("%d %s\n", count, name)
				color.Unset()
				err = syscall.PtraceSyscall(pid, 0)
				if err != nil {
				  	panic(err)
				}
    	}
		count += 1
		// We’ll get a SIGTRAP when this happens, which we need to wait for.
		_, err = syscall.Wait4(pid, nil, 0, nil)
		if err != nil {
			panic(err)
		}
		if count > 500 {
			break
		}
			
		exit = !exit
	}

	fmt.Println("Total syscall:", count)
}