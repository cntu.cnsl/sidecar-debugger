# sidecar-debugger
A project for using webHook to inject sidecar into a Kubernetes pod for debugging purpose.
This is a research project for my paper.

# Create webhook services
```
kubectl apply -f yaml/webhook.yaml
```

# CA request
```
kubectl exec -it -n sidecardebugger $(kubectl get pods --no-headers -o custom-columns=":metadata.name" -n sidecardebugger) -- wget -q -O- "127.0.0.1:8080/ca.pem?base64"
```

# References
Thank you for your shared knowledge and source code
* [1] https://github.com/wardviaene/kubernetes-course/blob/master/mutatingwebhook/
* [2] https://github.com/marcel-dempers/docker-development-youtube-series/tree/master/kubernetes/admissioncontrollers/
* [3] https://towardsdatascience.com/the-easiest-way-to-debug-kubernetes-workloads-ff2ff5e3cc75 Ephemeral containers 
