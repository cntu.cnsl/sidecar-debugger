#!/bin/bash
# Please deploy the experiment_nginx_sysadmin.yaml before this script
POD="experiment"
MAIN="main"
SIDECAR="sidecar"
MAIN_PROC_ID=$(kubectl exec --tty --stdin $POD -c $SIDECAR -- ps | grep daemon | awk -F' ' '{print $1}')
echo -e "\e[1;31m Check ID of the main container: \e[0m"
kubectl exec --request-timeout=2 --tty --stdin experiment -c main -- id
echo -e "\e[1;31m Try to change the hostname: \e[0m"
kubectl exec --request-timeout=2 --tty --stdin experiment -c main -- hostname new
echo -e "\e[1;31m Check ID of the chroot container: \e[0m"
kubectl exec --request-timeout=2 --tty --stdin experiment -c sidecar -- chroot /proc/$MAIN_PROC_ID/root id
echo -e "\e[1;31m Try to change the hostname: \e[0m"
OLD_NAME=$(kubectl exec --request-timeout=2 --tty --stdin experiment -c sidecar -- chroot /proc/$MAIN_PROC_ID/root hostname)
echo "Old name: $OLD_NAME"
kubectl exec --request-timeout=2 --tty --stdin experiment -c sidecar -- chroot /proc/$MAIN_PROC_ID/root hostname new
NEW_NAME=$(kubectl exec --request-timeout=2 --tty --stdin experiment -c sidecar -- chroot /proc/$MAIN_PROC_ID/root hostname)
echo "New name: $NEW_NAME"
kubectl exec --request-timeout=2 --tty --stdin experiment -c sidecar -- chroot /proc/$MAIN_PROC_ID/root hostname $OLD_NAME