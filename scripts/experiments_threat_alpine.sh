#!/bin/bash
POD="experiment"
MAIN="main"
SIDECAR="sidecar"
MAIN_PROC_ID=$(kubectl exec --tty --stdin $POD -c $SIDECAR -- ps | grep alpine | awk -F' ' '{print $1}')
CHROOT_OUT="/tmp/chroot.txt"
MAIN_OUT="/tmp/main.txt"
SIDECAR_OUT="/tmp/sidecar.txt"

k_chroot_command()
{
    COMMAND=$1
    PARAMS=$2
    OUTPUT=$3
    echo "kubectl exec --request-timeout=2 --tty --stdin $POD -c $SIDECAR -- chroot /proc/$MAIN_PROC_ID/root/ $COMMAND $PARAMS > $OUTPUT 2>/dev/null"
    kubectl exec --request-timeout=2 --tty --stdin $POD -c $SIDECAR -- chroot /proc/$MAIN_PROC_ID/root/ $COMMAND $PARAMS > $OUTPUT 2>/dev/null
}

k_chroot_command2()
{
    COMMAND=$1
    PARAMS=$2
    OUTPUT=$3
    echo "kubectl exec --request-timeout=2 --tty --stdin $POD -c $SIDECAR -- chroot /proc/$MAIN_PROC_ID/root/ $COMMAND $PARAMS"
    kubectl exec --request-timeout=2 --tty --stdin $POD -c $SIDECAR -- chroot /proc/$MAIN_PROC_ID/root/ $COMMAND $PARAMS 
}
k_main_command()
{
    COMMAND=$1
    PARAMS=$2
    OUTPUT=$3
    kubectl exec --request-timeout=2 --tty --stdin $POD -c $MAIN -- $COMMAND $PARAMS > $OUTPUT 2>/dev/null
}
k_sidecar_command() 
{
    COMMAND=$1
    PARAMS=$2
    OUTPUT=$3
    kubectl exec --request-timeout=2 --tty --stdin $POD -c $SIDECAR -- $COMMAND $PARAMS > $OUTPUT 2>/dev/null
}

check_command() 
{
    cmd=$1
    rm -f /tmp/chroot.txt /tmp/main.txt
    k_chroot_command $cmd "" $CHROOT_OUT 2>/dev/null 
    k_main_command $cmd "" $MAIN_OUT 2>/dev/null
    CHECK=$(diff $CHROOT_OUT $MAIN_OUT | wc -l)
    echo "Diff: $CHECK"
    if [ $CHECK -gt 0 ]
    then
        echo "STORING: ${cmd} to diff file"
        DIFF_CONTENT=$(diff $CHROOT_OUT $MAIN_OUT)
        echo "Diff: $DIFF_CONTENT"
        echo $cmd >> /tmp/bins_diff.txt
    fi
}
rm -f /tmp/bins.txt
k_chroot_command "echo" "/usr/bin/*" /tmp/bins.txt
rm -f /tmp/bins_diff.txt
dlen=$(wc -l /tmp/bins.txt | awk -F' ' '{print $1}')
echo "DOCUMENT_LEN: $dlen"
count=0
while :
do
    if [ $count -eq  $dlen ];
    then
        break
    fi
    # Get line
    count=$((count+1))
    line=$(sed -n ${count}p /tmp/bins.txt)
    for cmd in $line
    do
        if [ $cmd == "/bin/bashbug" ];
        then
            continue
        fi
        echo "CHECK_COMMAND: ${cmd##*/}"
        check_command ${cmd##*/}
    done
done