#!/bin/bash
# Collect cat function: Original
# time go run ../main.go debug --confine=0 cat ../README.md > ../experiments/proposed_model/original_write_cat.txt
# ## Collect cat function: Blocked
# time go run ../main.go debug --confine=1 cat ../README.md > ../experiments/proposed_model/blocked_write_cat.txt
# ## Collect cat function: enosys
# time go run ../main.go debug --confine=2 cat ../README.md > ../experiments/proposed_model/enosys_write_cat.txt
# ## Collect cat function: sysemu
# time go run ../main.go debug --confine=3 cat ../README.md > ../experiments/proposed_model/sysemu_write_cat.txt

# # Collect ls function: Original
# time go run ../main.go debug --confine=0 ls ../ > ../experiments/proposed_model/original_write_ls.txt
# # Collect ls function: Blocked
# time go run ../main.go debug --confine=1 ls ../ > ../experiments/proposed_model/blocked_write_ls.txt
# # Collect ls function: enosys
# time go run ../main.go debug --confine=2 ls ../ > ../experiments/proposed_model/enosys_write_ls.txt
# # Collect ls function: sysemu
# time go run ../main.go debug --confine=3 ls ../ > ../experiments/proposed_model/sysemu_write_ls.txt

# # Collect find function: Original
# time go run ../main.go debug --confine=0 find ../  > ../experiments/proposed_model/original_write_find.txt
# # Collect find function: Blocked
# time go run ../main.go debug --confine=1 find ../  > ../experiments/proposed_model/blocked_write_find.txt
# # Collect find function: enosys
# time go run ../main.go debug --confine=2 find ../  > ../experiments/proposed_model/enosys_write_find.txt
# # Collect find function: sysemu
# time go run ../main.go debug --confine=3 find ../  > ../experiments/proposed_model/sysemu_write_find.txt


# # Collect lsns function: Original
# time go run ../main.go debug --confine=0 lsns  > ../experiments/proposed_model/original_write_lsns.txt
# # Collect lsns function: Blocked
# time go run ../main.go debug --confine=1 lsns  > ../experiments/proposed_model/blocked_write_lsns.txt
# # Collect lsns function: enosys
# time go run ../main.go debug --confine=2 lsns  > ../experiments/proposed_model/enosys_write_lsns.txt
# # Collect lsns function: sysemu
# time go run ../main.go debug --confine=3 lsns  > ../experiments/proposed_model/sysemu_write_lsns.txt

# # Collect hostname function: Original
time go run ../main.go debug --confine=0 hostname  > ../experiments/proposed_model/original_write_hostname.txt
# Collect hostname function: Blocked
time go run ../main.go debug --confine=1 hostname  > ../experiments/proposed_model/blocked_write_hostname.txt
# Collect hostname function: enosys
time go run ../main.go debug --confine=2 hostname  > ../experiments/proposed_model/enosys_write_hostname.txt
# Collect hostname function: sysemu
time go run ../main.go debug --confine=3 hostname  > ../experiments/proposed_model/sysemu_write_hostname.txt